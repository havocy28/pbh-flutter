import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:my_app/main.dart';
import 'package:my_app/models/pet.dart';
import 'package:my_app/screens/login_page.dart';
import 'package:my_app/widgets/dialogue_builder.dart';
import 'package:transparent_image/transparent_image.dart';

class HomePage extends StatefulWidget {
  HomePage({this.jwt, this.payload});

  final String jwt;
  final Map<String, dynamic> payload;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var selectedPet;

  var petList = List<DropdownMenuItem>();

  @override
  void initState() {
    super.initState();
    _loadPets();
  }

  _loadPets() async {
    final response = await http.get('$SERVER_IP/pets/api/v2/pets/',
        headers: {HttpHeaders.authorizationHeader: "Bearer ${widget.jwt}"});
    final _pets = response.body;
    final _petList = jsonDecode(_pets);
    print(_petList);
    selectedPet = _petList['results'][0];
    _petList['results'].forEach((pet) {
      print(pet);
      setState(() {
        petList.add(
            DropdownMenuItem(child: Text(pet['name']), value: pet['name']));
        // DropdownMenuItem(child: Text(pet['name'])));
      });
    });

    petList.add(DropdownMenuItem(
        child: InkWell(onTap: () {}, child: Text('Add new pet')), value: ''));
  }

  String _petImageTest(String petImage) {
    if (petImage.isEmpty) {
      return "https://via.placeholder.com/150";
    }
    return petImage;
  }

  // Future<List<Pet>> _getPets() async {
  //   // print('token : $jwt');
  //   final response = await http.get('$SERVER_IP/pets/api/v2/pets/',
  //       headers: {HttpHeaders.authorizationHeader: "Bearer ${widget.jwt}"});
  //   // print(response.body);
  //   final test = response.body;
  //   final json = jsonDecode(test);
  //   final results = json['results'];
  //   final pets = results.map<Pet>((j) => Pet.fromJson(j)).toList();
  //   return pets;
  // }

  @override
  Widget build(BuildContext context) {
    // print(jwt);
    return Scaffold(
        appBar: AppBar(
          title: Text("Dashboard"),
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.close),
                onPressed: () async {
                  await storage.delete(key: "jwt");
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => LoginPage()));
                }),
          ],
        ),
        body: ListView(children: <Widget>[
          DropdownButtonFormField(
              value: selectedPet['name'],
              items: petList,
              // hint: Text('Select a pet'),
              onChanged: (changedValue) {
                setState(() {
                  selectedPet = changedValue;
                });
              }),
          Center(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CircleAvatar(
                radius: 80,
                backgroundImage: NetworkImage(selectedPet['pet_image']),
              ),
            ],
          ))
        ]));
  }
}

// FutureBuilder<List<Pet>>(
//   // {"Authorization" : "Bearer $jwt"}
//   future: _getPets(),
//   builder: (BuildContext context, AsyncSnapshot<List<Pet>> snapshot) {
//     // Check for nothing/loading
//     // snapshot.connectionState
//     if (snapshot.hasData == false) {
//       //Future is not completed
//       // Show loading UI
//       return Center(child: CupertinoActivityIndicator());
//     }

//     final pets = snapshot.data;

//     if (pets.isEmpty) {
//       return Text('No Pets', style: Theme.of(context).textTheme.headline);
//     }

//     return ListView.builder(
//         itemCount: snapshot.data.length,
//         itemBuilder: (context, index) {
//           final pet = pets[index];
//           return Padding(
//             padding: const EdgeInsets.all(8.0),
//             child: ListTile(
//               title: Text(pet.name,
//                   style: Theme.of(context).textTheme.headline),
//               onTap: () => showDialog(
//                   context: context,
//                   builder: (context) => dialogueBuilder(context, pet)),
//             ),
//           );
//         });
//   },
// ),
