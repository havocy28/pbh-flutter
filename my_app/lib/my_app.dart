import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:my_app/main.dart';
import 'package:my_app/screens/home_page.dart';
import 'package:my_app/screens/login_page.dart';

class MyApp extends StatelessWidget {
  Future<String> get jwtOrEmpty async {
    var jwt = await storage.read(key: "jwt");
    if(jwt == null) return "";
    return jwt;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Authentication Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: FutureBuilder(
        future: jwtOrEmpty,            
        builder: (context, snapshot) {
          if(!snapshot.hasData) return CircularProgressIndicator();
          if(snapshot.data != "") {
            var str = snapshot.data;
            var jwt = str.split(".");

            // eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNTgzNDc1NzIwLCJqdGkiOiJhYTVkYThiMmQ2Yjk0NzE2YTVjMDIxN2E0ZDdiNmRlNiIsInVzZXJfaWQiOjI1fQ.rX-ebWb2Pg_ib329EWypOACvP66SX7FNH4wIZDL7MqE
            // sample token
// eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNTgzNDc0NDE2LCJqdGkiOiI0ZjE1ODc1Njc5OTk0MzliYmMwZDQwZmQyNzVjYmNhNyIsInVzZXJfaWQiOjI1fQ.P0R2THVnFmvf5Tf3AWEQSjxaDGIOQqwG3IA_hN4avPg
            if(jwt.length !=3) {
              return LoginPage();
            } else {
              var payload = json.decode(ascii.decode(base64.decode(base64.normalize(jwt[1]))));
              if(DateTime.fromMillisecondsSinceEpoch(payload["exp"]*1000).isAfter(DateTime.now())) {
                return HomePage(jwt: str, payload: payload);
              } else {
                return LoginPage();
              }
            }
          } else {
            return LoginPage();
          }
        }
      ),
    );
  }
}
