import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'dart:convert' show ascii, base64, json, jsonDecode, jsonEncode;

import 'my_app.dart';

const SERVER_IP = 'http://10.0.2.2:8000';
final storage = FlutterSecureStorage();

void main() {
  runApp(MyApp());
}


