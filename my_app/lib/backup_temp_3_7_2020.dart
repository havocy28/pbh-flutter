// import 'dart:io';

// import 'package:flutter/material.dart';
// import 'package:http/http.dart' as http;
// import 'package:flutter_secure_storage/flutter_secure_storage.dart';
// import 'dart:convert' show json, base64, ascii, jsonDecode;

// const SERVER_IP = 'http://10.0.2.2:8000';
// final storage = FlutterSecureStorage();

// void main() {
//   runApp(MyApp());
// }

// class MyApp extends StatelessWidget {
//   Future<String> get jwtOrEmpty async {
//     var jwt = await storage.read(key: "jwt");
//     if(jwt == null) return "";
//     return jwt;
//   }

//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Authentication Demo',
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//       ),
//       home: FutureBuilder(
//         future: jwtOrEmpty,            
//         builder: (context, snapshot) {
//           if(!snapshot.hasData) return CircularProgressIndicator();
//           if(snapshot.data != "") {
//             var str = snapshot.data;
//             var jwt = str.split(".");

//             // eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNTgzNDc1NzIwLCJqdGkiOiJhYTVkYThiMmQ2Yjk0NzE2YTVjMDIxN2E0ZDdiNmRlNiIsInVzZXJfaWQiOjI1fQ.rX-ebWb2Pg_ib329EWypOACvP66SX7FNH4wIZDL7MqE
//             // sample token
// // eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNTgzNDc0NDE2LCJqdGkiOiI0ZjE1ODc1Njc5OTk0MzliYmMwZDQwZmQyNzVjYmNhNyIsInVzZXJfaWQiOjI1fQ.P0R2THVnFmvf5Tf3AWEQSjxaDGIOQqwG3IA_hN4avPg
//             if(jwt.length !=3) {
//               return LoginPage();
//             } else {
//               var payload = json.decode(ascii.decode(base64.decode(base64.normalize(jwt[1]))));
//               if(DateTime.fromMillisecondsSinceEpoch(payload["exp"]*1000).isAfter(DateTime.now())) {
//                 return HomePage(jwt: str, payload: payload);
//               } else {
//                 return LoginPage();
//               }
//             }
//           } else {
//             return LoginPage();
//           }
//         }
//       ),
//     );
//   }
// }

// class LoginPage extends StatelessWidget {
//   final TextEditingController _usernameController = TextEditingController();
//   final TextEditingController _passwordController = TextEditingController();

//   void displayDialog(context, title, text) => showDialog(
//       context: context,
//       builder: (context) =>
//         AlertDialog(
//           title: Text(title),
//           content: Text(text)
//         ),
//     );

//   Future<String> attemptLogIn(String username, String password) async {
//     // print("$SERVER_IP/api/token/");
//     var res = await http.post(
//       "$SERVER_IP/api/token/",
//       body: {
//         "username": username,
//         "password": password
//       }
//     );
//     // print(res.statusCode);
//     // print(res.body);
//     // if(res.statusCode == 200) return res.body;
//     if(res.statusCode == 200) return json.decode(res.body)['access'];
//     return null;
//   }

//   Future<int> attemptSignUp(String username, String password) async {
//     var res = await http.post(
//       '$SERVER_IP/api/jwtauth/register/',
//       body: {
//         "username": username,
//         "password": password
//       }
//     );
//     return res.statusCode;
    
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(title: Text("Log In"),),
//       body: Padding(
//         padding: const EdgeInsets.all(8.0),
//         child: Column(
//           children: <Widget>[
//             TextField(
//               controller: _usernameController,
//               decoration: InputDecoration(
//                 labelText: 'Username'
//               ),
//             ),
//             TextField(
//               controller: _passwordController,
//               obscureText: true,
//               decoration: InputDecoration(
//                 labelText: 'Password'
//               ),
//             ),
//             FlatButton(
//               onPressed: () async {
//                 var username = _usernameController.text;
//                 var password = _passwordController.text;
//                 var jwt = await attemptLogIn(username, password);
//                 if(jwt != null) {
//                   storage.write(key: "jwt", value: jwt);
//                   Navigator.push(
//                     context,
//                     MaterialPageRoute(
//                       builder: (context) => HomePage(jwt: jwt)
//                     )
//                   );
//                 } else {
//                   displayDialog(context, "An Error Occurred", "No account was found matching that username and password");
//                 }
//               },
//               child: Text("Log In")
//             ),
//             FlatButton(
//               onPressed: () async {
//                 var username = _usernameController.text;
//                 var password = _passwordController.text;

//                 if(username.length < 4) 
//                   displayDialog(context, "Invalid Username", "The username should be at least 4 characters long");
//                 else if(password.length < 4) 
//                   displayDialog(context, "Invalid Password", "The password should be at least 4 characters long");
//                 else{
//                   var res = await attemptSignUp(username, password);
//                   if(res == 201)
//                     displayDialog(context, "Success", "The user was created. Log in now.");
//                   else if(res == 409)
//                     displayDialog(context, "That username is already registered", "Please try to sign up using another username or log in if you already have an account.");  
//                   else {
//                     displayDialog(context, "Error", "An unknown error occurred.");
//                   }
//                 }
//               },
//               child: Text("Sign Up")
//             )
//           ],
//         ),
//       )
//     );
//   }
// }

// class HomePage extends StatelessWidget {
//   HomePage({this.jwt, this.payload});

//   final String jwt;
//   final Map<String, dynamic> payload;

//   _getPets() async {
//     // print('token : $jwt');
//     final response = await http.get('$SERVER_IP/pets/api/v2/pets/', headers: {HttpHeaders.authorizationHeader: "Bearer $jwt"});
//     // print(response.body);
//   }

//   @override
//   Widget build(BuildContext context) {
//     // print(jwt);
//     return Scaffold(
//       appBar: AppBar(title: Text("Secret Data Screen"),
//       actions: <Widget>[
//         IconButton(icon: Icon(Icons.close), onPressed: () async {
//           await storage.delete(key: "jwt");
//           Navigator.push(context, MaterialPageRoute(builder: (context) => LoginPage()));
//         })
//       ],
//       ),
//       body: Center(
//         child: FutureBuilder(
//           // {"Authorization" : "Bearer $jwt"}
//           future: _getPets(),
//           builder: (context, snapshot) {
//             return Card(child: Text("Hi"));
//             // print(snapshot.data);
//             // snapshot.hasData ?
//             // Column(children: <Widget>[
//             //   Text("${payload['username']}, here's the data:"),
//             //   Text(snapshot.data, style: Theme.of(context).textTheme.display1)
//             // ],)
//             // :
//             // snapshot.hasError ? Text("An error occurred") : CircularProgressIndicator()
//           }
//         ),
//       ),
//     );
//   }
// }
// //TODO add git repo