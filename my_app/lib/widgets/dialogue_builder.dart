import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_app/models/pet.dart';
import 'package:transparent_image/transparent_image.dart';

@override
Widget dialogueBuilder(BuildContext context, Pet pet) {
  ThemeData localTheme = Theme.of(context);

  return SimpleDialog(
    contentPadding: EdgeInsets.zero,
    children: [
      if (pet.petImage.isNotEmpty)
        FadeInImage.memoryNetwork(
            // print(pet.petImage),
            placeholder: kTransparentImage,
            image: pet.petImage,
            fit: BoxFit.fill),
      Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            Text(
              pet.name,
              style: localTheme.textTheme.display1,
            ),
            Text(pet.speciesName),
          ],
        ),
      ),
    ],
  );
}
