class Pet{
  final int id;
  final String name;
  final int species;
  final String speciesName;
  final String petImage;
  final int belongsTo;

  Pet.fromJson(Map json):
  id=json['id'], name=json['name'], speciesName=json['species_name'], species=json['species'], petImage=json['pet_image'],
  belongsTo=json['belongs_to'];


}