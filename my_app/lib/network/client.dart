import 'dart:io';
import 'package:http/http.dart' as http;


const SERVER_IP = 'http://10.0.2.2:8000';

class Client{
  Future get(String path) async {
    final response = await http.get('$SERVER_IP/$path',
        headers: {HttpHeaders.authorizationHeader: "Bearer ${widget.jwt}"});
    final _pets = response.body;
    final _petList = jsonDecode(_pets);
    print(_petList);
    selectedPet = _petList['results'][0];
    _petList['results'].forEach((pet) {
      print(pet);
      setState(() {
        petList.add(
            DropdownMenuItem(child: Text(pet['name']), value: pet['name']));
        // DropdownMenuItem(child: Text(pet['name'])));
      });
    });

    petList.add(DropdownMenuItem(
        child: InkWell(onTap: () {}, child: Text('Add new pet')), value: ''));
  }
}


